export function init(pluginAPI) {
  var iconProvider = pluginAPI.require("iconProvider");

  var objectsIconPath = pluginAPI.getPath() + "icons/objects/";
  var OAAMView = function (iconPath) {
    var self = this;
    this.iconPath = iconPath;
    this.apply = function (data) {
      var view = {};
      if (data.type == "eObject") {
        switch (data.value.eClass.get("name")) {
          case "EReference":
            if (data.value.get("containment")) {
              return {
                title: data.value.get("name"),
                icon: iconProvider.getPathToIcon(data.value.get("eType")),
                unselectable: true,
                eObject: data.value,
                modifiers: data.modifiers,
                lazy: true,
              };
            } else {
              return null;
            }
          case "EAttribute":
            return null;
          default:
            var objectName = "";
            if (data.value.eClass.getEStructuralFeature("name")) {
              if (data.value.get("name")) {
                objectName = "'" + data.value.get("name") + "'";
              }
            }

            return {
              title: data.value.eClass.get("name") + " " + objectName,
              icon: iconProvider.getPathToIcon(data.value.eClass),
              unselectable: false,
              eObject: data.value,
              modifiers: data.modifiers,
              lazy: true,
            };
        }
      }
    };

    this.canDisplay = function (data) {
      if (data.type != "eObject") {
        return false;
      } else {
        var nsURI = "http://www.oaam.de/oaam/model/v160";
        switch (data.value.eClass.get("name")) {
          case "EReference":
            return data.value.eContainer.eContainer.get("nsURI").includes(nsURI);
          case "EAttribute":
            return data.value.eContainer.eContainer.get("nsURI").includes(nsURI);
          default:
            return data.value.eClass.eContainer.get("nsURI").includes(nsURI);
        }
      }
    };
  };

  var OAAMController = function (ecoreSync) {
    var ecoreSync = ecoreSync;

    this.canLoad = function (data) {
      switch (data.node.data.eObject.eClass.get("name")) {
        case "EReference":
          return data.node.data.eObject.eContainer.eContainer
            .get("nsURI")
            .includes("http://www.oaam.de/oaam/model/v160");
        case "EAttribute":
          return data.node.data.eObject.eContainer.eContainer
            .get("nsURI")
            .includes("http://www.oaam.de/oaam/model/v160");
        default:
          return data.node.data.eObject.eClass.eContainer
            .get("nsURI")
            .includes("http://www.oaam.de/oaam/model/v160");
      }
    };

    this.onUpdate = function (parentEObject, eObject, callbackFcn) {
      var self = this;
      if (eObject.eClass.get("name") == "EReference") {
        var update = () => {
          self.load(parentEObject, eObject).then(function (loadedData) {
            callbackFcn(loadedData);
          });
        };

        if (eObject.get("upperBound") == 1) {
          parentEObject.on("change:" + eObject.get("name"), function () {
            update();
          });

          parentEObject.on("unset:" + eObject.get("name"), function () {
            update();
          });
        } else {
          parentEObject.on("add:" + eObject.get("name"), function () {
            update();
          });
          parentEObject.on("remove:" + eObject.get("name"), function () {
            update();
          });
        }
      }
    };

    this.onChange = function (data, callbackFcn) {
      if (data.type == "eObject") {
        data.value.on("change", function () {
          callbackFcn(data.value);
        });
      }
    };

    this.load = function (parent, eObject) {
      var load = [];
      switch (eObject.eClass.get("name")) {
        case "EReference":
          load = ecoreSync.get(parent, eObject.get("name")).then(function (results) {
            //Make results array
            var value = [];
            if (Array.isArray(results)) {
              value = results;
            } else {
              if (results != null) {
                value = [results];
              }
            }

            return Promise.all(
              value.map(async function (e) {
                if (e.eClass.getEStructuralFeature("name") || true) {
                  //preloading the name of EObjects where applicable
                  //TODO: accelerate with compound command
                  return ecoreSync.get(e, "name");
                } else {
                  return null;
                }
              }),
            ).then(function () {
              return Promise.resolve(
                value.map(function (e) {
                  return { type: "eObject", value: e, modifiers: {} };
                }),
              );
            });
          });
          break;
        case "EAttribute":
          load = ecoreSync.get(parent, eObject.get("name")).then(function (value) {
            return Promise.resolve([
              {
                type: "value",
                value: {
                  dataType: eObject.get("eType").get("name"),
                  value: value,
                  modifiers: {},
                },
              },
            ]);
          });
          break;
        default:
          load = ecoreSync.utils.isEClassInitialized(eObject.eClass).then(function () {
            var features = eObject.eClass.get("eStructuralFeatures").array();
            return Promise.resolve(
              features.map(function (e) {
                return {
                  type: "eObject",
                  value: e,
                  modifiers: { eOwner: eObject },
                };
              }),
            );
          });
      }

      return load;
    };
  };

  var extensions = {};
  extensions.contextMenu = pluginAPI.provide("plugin.ecoreTreeView.oaam.contextMenu");

  var OAAMContextMenu = function (ecoreSync) {
    var self = this;
    this.ecoreSync = ecoreSync;
    this.orderPriority = 1;
    this.canDisplayMenuFor = function (data) {
      if (!data.eObject) {
        return false;
      } else {
        var nsURI = "http://www.oaam.de/oaam/model/v160";
        switch (data.eObject.eClass.get("name")) {
          case "EReference":
            return data.eObject.eContainer.eContainer.get("nsURI").includes(nsURI);
          case "EAttribute":
            return data.eObject.eContainer.eContainer.get("nsURI").includes(nsURI);
          default:
            return data.eObject.eClass.eContainer.get("nsURI").includes(nsURI);
        }
      }
    };

    this.display = function (node) {
      var data = node.data;
      switch (data.eObject.eClass.get("name")) {
        case "EReference":
          var creationPossible = function () {
            var refContents = data.modifiers["eOwner"].get(data.eObject.get("name"));
            if (refContents) {
              if (
                data.eObject.get("upperBound") == 1 ||
                refContents.length == data.eObject.get("upperBound")
              ) {
                return false;
              } else {
                return true;
              }
            } else {
              return true;
            }
          };

          var styleDisabled = "";
          if (!creationPossible()) {
            //TODO: use CSS class
            styleDisabled =
              ' style="filter: grayscale(100%); -webkit-filter: grayscale(100%); opacity:0.5;" ';
          }

          if (data.eObject.get("eType").get("abstract")) {
            var concreteClasses = ecoreSync.__findConcreteClasses(data.eObject.get("eType"));
            var creatableItems = concreteClasses.map(function (e) {
              return {
                name:
                  e.get("name") +
                  ' <img src="' +
                  objectsIconPath +
                  e.get("name") +
                  '.gif" ' +
                  styleDisabled +
                  ">",
                isHtmlName: true,
                icon: "add",
                action: false,
                disabled: !creationPossible(),
                callback: async function () {
                  ecoreSync.create(e).then(function (eObject) {
                    ecoreSync.add(data.modifiers["eOwner"], data.eObject.get("name"), eObject);
                  });
                },
              };
            });
            return {
              callback: function (key, options) {
                //TODO: menu actions
              },
              items: {
                create: {
                  name: "Create",
                  icon: "add",
                  action: false,
                  items: creatableItems,
                },
                paste: {
                  name: "Paste",
                  icon: "paste",
                  disabled: function () {
                    return true;
                  },
                },
                copy: { name: "Copy All", icon: "copy" },
                cut: { name: "Cut All", icon: "cut" },
                clear: { name: "Remove All", icon: "delete" },
              },
            };
          } else {
            return {
              callback: function (key, options) {
                //TODO: menu actions
                if ($DEBUG) console.error("attempting to clear reference");
                if (key == "create") {
                  ecoreSync.create(data.eObject.get("eType")).then(function (eObject) {
                    ecoreSync
                      .add(data.modifiers["eOwner"], data.eObject.get("name"), eObject)
                      .then(function () {
                        if ($DEBUG)
                          console.debug("eObject successfully created and added to reference");
                      });
                  });
                }

                if (key == "clear") {
                  ecoreSync
                    .get(data.modifiers.eOwner, data.eObject.get("name"))
                    .then(function (results) {
                      return Promise.all(
                        results.map(function (e) {
                          return ecoreSync.remove(
                            data.modifiers.eOwner,
                            data.eObject.get("name"),
                            e,
                          );
                        }),
                      ).then(function () {
                        if ($DEBUG) console.debug("eReference successfully cleared");
                        return Promise.resolve();
                      });
                    });
                }
              },
              items: {
                create: {
                  name:
                    "Create " +
                    data.eObject.get("eType").get("name") +
                    ' <img src="' +
                    objectsIconPath +
                    data.eObject.get("eType").get("name") +
                    '.gif" ' +
                    styleDisabled +
                    ">",
                  isHtmlName: true,
                  icon: "add",
                  disabled: !creationPossible(),
                },
                paste: {
                  name: "Paste",
                  icon: "paste",
                  disabled: function () {
                    return true;
                  },
                },
                copy: { name: "Copy All", icon: "copy" },
                cut: { name: "Cut All", icon: "cut" },
                clear: { name: "Remove All", icon: "delete" },
              },
            };
          }
        case "EAttribute":
          return false;
        default:
          return {
            callback: function (key, options) {
              //TODO: menu actions
              if (key == "delete") {
                ecoreSync
                  .remove(
                    data.eObject.eContainer,
                    data.eObject.eContainingFeature.get("name"),
                    data.eObject,
                  )
                  .then(function () {
                    if ($DEBUG) console.debug("eObject successfully removed from reference");
                  });
              }
            },
            items: {
              copy: { name: "Copy", icon: "copy" },
              cut: { name: "Cut", icon: "cut" },
              delete: { name: "Remove", icon: "delete" },
            },
          };
      }
    };
  };

  //This registers the creation of OAAM models with the workspace

  var OAAMWorkspaceCreationMenu = function (ecoreSync) {
    this.orderPriority = 2;
    this.canDisplayMenuFor = function (data) {
      if (!data.eObject) {
        return false;
      } else {
        var nsURI = "http://www.eoq.de/workspacemdbmodel/v1.0";
        return (
          data.eObject.eClass.eContainer.get("nsURI").includes(nsURI) &&
          (data.eObject.eClass.get("name") == "Directory" ||
            data.eObject.eClass.get("name") == "Workspace")
        );
      }
    };
    this.display = function (node) {
      var data = node.data;

      var creatableWorkspaceItems = {
        "oaam-model-create": {
          name: "OAAM Model",
          icon: "oaam",
          callback: async function () {
            var resourceNames = await ecoreSync.exec(
              new eoq2.Get(new eoq2.Obj(data.eObject.get("_#EOQ")).Pth("resources").Pth("name")),
            );

            console.error(resourceNames);

            var resBaseName = "newOAAMmodel";
            var currentResName = resBaseName;
            var collisions = 0;
            var nameFound = false;
            while (!nameFound) {
              var nameTaken = false;

              for (let n in resourceNames) {
                if (resourceNames[n] == currentResName + ".oaam") {
                  nameTaken = true;
                  break;
                }
              }

              if (nameTaken) {
                currentResName = resBaseName + (collisions + 1);
                collisions += 1;
              } else {
                nameFound = true;
              }
            }

            var cmd = new eoq2.Cmp();
            cmd
              .Crn("http://www.eoq.de/workspacemdbmodel/v1.0", "ModelResource", 1)
              .Set(new eoq2.Qry().His(-1), "name", currentResName + ".oaam")
              .Crn("http://www.oaam.de/oaam/model/v160", "Architecture")
              .Add(new eoq2.His(0), "contents", new eoq2.His(-1))
              .Add(
                new eoq2.Qry().Obj(data.eObject.get("_#EOQ")),
                "resources",
                new eoq2.Qry().His(0),
              );

            ecoreSync.exec(cmd);
          },
        },
      };
      return {
        callback: function (key, options) {},
        items: {
          create: {
            name: "New",
            icon: "add",
            action: false,
            items: creatableWorkspaceItems,
          },
        },
      };
    };
  };

  pluginAPI.implement("plugin.ecoreTreeView.views", OAAMView);
  pluginAPI.implement("plugin.ecoreTreeView.controllers", OAAMController);
  pluginAPI.implement("plugin.ecoreTreeView.menus", OAAMContextMenu);
  pluginAPI.implement("plugin.ecoreTreeView.menus", OAAMWorkspaceCreationMenu);
  return Promise.resolve();
}

export var meta = {
  id: "plugin.ecoreTreeView.oaam",
  description: "Ecore Tree View for OAAM",
  author: "Matthias Brunner",
  version: "0.0.1",
  requires: ["plugin.ecoreTreeView", "eventBroker"],
};
